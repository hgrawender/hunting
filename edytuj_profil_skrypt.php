<?php

    session_start();
    
    if ((!isset($_POST['Imie'])) || (!isset($_POST['Nazwisko'])) || (!isset($_POST['Kod_pocztowy'])) || (!isset($_POST['Ulica'])) || (!isset($_POST['Telefon'])) || (!isset($_POST['Miejscowosc'])) || (!isset($_POST['Data_dolaczenia'])))
    {
        header('Location: edytuj_profil.php');
        exit();
    }

    require_once "connect.php";

    $polaczenie = @new mysqli($host, $db_user, $db_password, $db_name);
    $polaczenie->set_charset("utf8");
    
    if ($polaczenie->connect_errno!=0)
    {
        echo "Error: ".$polaczenie->connect_errno;
    }
    else
    {
        $Imie=$_POST['Imie'];
        $Nazwisko=$_POST['Nazwisko'];
        $Email=$_POST['Email'];
        $Kod_pocztowy=$_POST['Kod_pocztowy'];
        $Ulica=$_POST['Ulica'];
        $Telefon=$_POST['Telefon'];
        $Miejscowosc=$_POST['Miejscowosc'];
        $Data_dolaczenia=$_POST['Data_dolaczenia'];
        $id = $_SESSION['ID'];

        $_SESSION['Imie'] = $_POST['Imie'];
        $_SESSION['Nazwisko'] = $_POST['Nazwisko'];
        $_SESSION['Ulica'] = $_POST['Ulica'];
        $_SESSION['Kod_pocztowy'] = $_POST['Kod_pocztowy'];
        $_SESSION['Telefon'] = $_POST['Telefon'];
        $_SESSION['Email'] = $_POST['Email'];
        $_SESSION['Data_dolaczenia'] = $_POST['Data_dolaczenia'];


        $Imie= htmlentities($Imie, ENT_QUOTES, "UTF-8");
        $Nazwisko= htmlentities($Nazwisko, ENT_QUOTES, "UTF-8");
        $Email= htmlentities($Email, ENT_QUOTES, "UTF-8");
        $Kod_pocztowy= htmlentities($Kod_pocztowy, ENT_QUOTES, "UTF-8");
        $Ulica= htmlentities($Ulica, ENT_QUOTES, "UTF-8");
        $Telefon= htmlentities($Telefon, ENT_QUOTES, "UTF-8");
        //$Miejscowosc= htmlentities( $Miejscowosc, ENT_QUOTES, "UTF-8");   //Genereuje brak o z kreską
        $Data_dolaczenia= htmlentities( $Data_dolaczenia, ENT_QUOTES, "UTF-8");

    

                                    
                                    // $polaczenie->query("UPDATE MYSLIWI SET Imie = '$Imie', Nazwisko='$Nazwisko', Ulica='$Ulica', Miejscowosc='$Miejscowosc', Kod_pocztowy='$Kod_pocztowy', Telefon= '$Telefon', Email='$Email', Data_dolaczenia='$Data_dolaczenia' WHERE ID = $id;");




        if ($rezultat = $polaczenie->query(
        sprintf("UPDATE MYSLIWI SET Imie = '%s', Nazwisko='%s', Ulica='%s', Miejscowosc='%s', Kod_pocztowy='%s', Telefon= '%s', Email='%s', Data_dolaczenia='%s' WHERE ID = '%s';",
        mysqli_real_escape_string($polaczenie,$Imie),                      //zabezpieczenie przed SQL inj
        mysqli_real_escape_string($polaczenie,$Nazwisko),
        mysqli_real_escape_string($polaczenie,$Ulica),
        mysqli_real_escape_string($polaczenie,$Miejscowosc),
        mysqli_real_escape_string($polaczenie,$Kod_pocztowy),
        mysqli_real_escape_string($polaczenie,$Telefon),
        mysqli_real_escape_string($polaczenie,$Email),
        mysqli_real_escape_string($polaczenie,$Data_dolaczenia),
        mysqli_real_escape_string($polaczenie,$id))))


        
        header('Location: profil.php?z=1');
        
        $polaczenie->close();
    }
    
?>
