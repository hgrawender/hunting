<?php
	session_start();
	require_once "connect.php";
	$polaczenie = new mysqli($host, $db_user, $db_password, $db_name);
	$polaczenie->set_charset("utf8");
?>


<!DOCTYPE html>
<html lang="pl">
<head>
	<meta charset="UTF-8">
	<title>Dodaj skup! - PHP</title>
	

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<script src="js/bootstrap.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Exo" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="main.css"/>
	<link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.css"/>

</script>
</head>
<body>
<?php include 'header.php'; ?>




	
<div class="container">
<h1>>Dodaj skup</h1>
	<form class="well form-horizontal" action="" method="post" >
	<fieldset>
		<legend>Proszę wypełnij poniższy formularz:</legend>	

	<div class="form-group">
 		 <label class="col-md-4 control-label" >Nazwa skupu:</label> 
    	<div class="col-md-4 inputGroupContainer">
    		<div class="input-group">
  				<span class="input-group-addon"><i class="glyphicon glyphicon-tags"></i></span>
  					<input type="text" id="nazwa" name="nazwa" class="form-control" placeholder="...">
    		</div>
  		</div>
	</div>
	
	<div class="form-group">
 		 <label class="col-md-4 control-label" >Adres:</label> 
    	<div class="col-md-4 inputGroupContainer">
    		<div class="input-group">
  				<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
  					<input type="text" id="adres" name="adres" class="form-control" placeholder="...">
    		</div>
  		</div>
	</div>

	<div class="form-group">
 		 <label class="col-md-4 control-label" >Miasto:</label> 
    	<div class="col-md-4 inputGroupContainer">
    		<div class="input-group">
  				<span class="input-group-addon">
  					<i class="fa fa-building" aria-hidden="true" style="font-size: 16px;"></i></span>
  					<input type="text" id="miasto" name="miasto" class="form-control" placeholder="...">
    		</div>
  		</div>
  	</div>

	<div class="form-group">
 		<label class="col-md-4 control-label" >Kod pocztowy:</label> 
    	<div class="col-md-4 inputGroupContainer">
    		<div class="input-group">
  				<span class="input-group-addon"><i class="glyphicon glyphicon-tags"></i></span>
  					<input type="text" id="kod_pocztowy" name="kod_pocztowy" class="form-control" placeholder="...">
    		</div>
  		</div>
	</div>

	<div class="form-group">
 		 <label class="col-md-4 control-label" >NIP:</label> 
    	<div class="col-md-4 inputGroupContainer">
    		<div class="input-group">
  				<span class="input-group-addon">
  					<i class="fa fa-address-card-o" aria-hidden="true"></i></span>
  					<input type="text" id="nip" name="nip" class="form-control" placeholder="...">
    		</div>
  		</div>
	</div>

	<div class="form-group">
 		 <label class="col-md-4 control-label" >Telefon:</label> 
    	<div class="col-md-4 inputGroupContainer">
    		<div class="input-group">
  				<span class="input-group-addon"><i class="glyphicon glyphicon-phone-alt"></i></span>
  					<input type="text" id="telefon" name="telefon" class="form-control" placeholder="...">
    		</div>
  		</div>
	</div>

	<div class="form-group">
 		 <label class="col-md-4 control-label" >E-mail:</label> 
    	<div class="col-md-4 inputGroupContainer">
    		<div class="input-group">
  				<span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
  					<input type="text" id="email" name="email" class="form-control" placeholder="...">
    		</div>
  		</div>
	</div>
<hr>
	  	
	  		<span class="pull-right">
		    	<button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok"></span> &nbsp;Dodaj skup do bazy </button>
		    	<a href="skupy.php" class="btn btn-warning">Anuluj</a>
		    </span> 


</fieldset>
</form>
</div>


<?php
echo "<br/><h3> Sekcja do testowania formularza - przekazane dane:</h3><h4>zapytaj o problem z enterem</h4>";



		if (isset($_POST['nazwa']))
		{
			echo "<br/>Nazwa skupu: ";
			echo $_POST['nazwa'];
			$nazwa=$_POST['nazwa'];

			if (isset($_POST['adres']))
			{
				echo "<br/>adres: ";
				echo $_POST['adres'];
				$adres=$_POST['adres'];

				if (isset($_POST['miasto']))
				{
					echo "<br/>miasto: ";
					echo $_POST['miasto'];
					$miasto=$_POST['miasto'];

					if (isset($_POST['kod_pocztowy']))
					{
						echo "<br/>kod pocztowy: ";
						echo $_POST['kod_pocztowy'];
						$kod_pocztowy=$_POST['kod_pocztowy'];

						if (isset($_POST['nip']))
						{
							echo "<br/>nip: ";
							echo $_POST['nip'];
							$nip=$_POST['nip'];

							if (isset($_POST['telefon']))
							{
							echo "<br/>telefon: ";
							echo $_POST['telefon'];
							$telefon=$_POST['telefon'];

								if (isset($_POST['email']))
								{
								echo "<br/>email: ";
								echo $_POST['email'];
								$email=$_POST['email'];


								$polaczenie->query("INSERT INTO SKUPY (ID, Nazwa, Ulica, Miejscowosc, Kod_pocztowy, NIP, Telefon, Email) 
									VALUES (DEFAULT, '$nazwa', '$adres', '$miasto', '$kod_pocztowy', '$nip', '$telefon', '$email')");

								echo "INSERT INTO SKUPY (ID, Nazwa, Ulica, Miejscowosc, Kod_pocztowy, NIP, Telefon, Email) 
									VALUES (DEFAULT, '$nazwa', '$adres', '$miasto', '$kod_pocztowy', '$nip', '$telefon', '$email')";
								$polaczenie->close();
								}else echo "Email nie został ustawiony";
							}else echo "Telefon nie został ustawiony";
						}else echo "Nip nie został ustawiony";
					}else echo "Kod pocztowy nie został ustawiony";
				}else echo "Miasto nie zostało ustawione";
			}else echo "Adres nie został ustawiony";
		}else echo "Nazwa skupu nie została ustawiona";



	$polaczenie->close();
?>

</body>
</html>