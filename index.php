<?php

	session_start();
	if ((isset($_SESSION['zalogowany'])) && ($_SESSION['zalogowany']==true))
	{
		header('Location: profil.php');
		exit();
	}

?>

<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Logowanie</title>

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<script src="js/bootstrap.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Exo" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="main.css"/>
</head>

<body>
	
	<div class="container">
		<h1>>Logowanie</h1>
	    <form class="well form-horizontal" method="post" action="zaloguj.php" >
			<fieldset>
				<legend>Zaloguj się:</legend>	
			
				<div class="form-group">
		 			<label class="col-md-4 control-label" >E-mail:</label> 
			    	<div class="col-md-4 inputGroupContainer">
			    		<div class="input-group">
			  				<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
			  					<input type="text" name="email" class="form-control">
			    		</div>
			  		</div>
				</div>

				<div class="form-group">
		 			<label class="col-md-4 control-label" >Hasło:</label> 
			    	<div class="col-md-4 inputGroupContainer">
			    		<div class="input-group">
			  				<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
			  					<input type="password" name="haslo" class="form-control">
			    		</div>
			  		</div>
				</div>

				<div class="form-group">
				  <label class="col-md-5 control-label "></label>
				  	<div class="col-md-4" >
				    	<button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok"></span> &nbsp;Zaloguj się </button>
				  	</div>
				</div>
			</fieldset>
		</form>
	</div>

<div style="text-align: center"> 
<?php
	if(isset($_SESSION['blad']))	echo $_SESSION['blad'];
?>
</div>
</body>
</html>