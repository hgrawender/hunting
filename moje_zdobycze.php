<?php

	session_start();
	
	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
	
?>

<?php
    require_once "connect.php";
    $polaczenie = new mysqli($host, $db_user, $db_password, $db_name);
    $polaczenie->set_charset("utf8");
?>

<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Zalogowano</title>

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<script src="js/bootstrap.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Exo" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="main.css"/>
</head>

<body>
<?php include 'header.php'; ?>





	<div class="container">
		<h1>>Zdobycze</h1>
		<div class="well form-horizontal">
			<fieldset>
<?php
	echo '<legend>Witaj '.$_SESSION['Imie'].', twoje zdobycze to:</legend>';	
?>


    <div class=" col-md-12 col-lg-12 "> 

	<table id="table" class="table table-hover table-mc-light-blue" style="text-align:center;">
      <thead>
        <tr>
          <th>Gatunek</th>
          <th>Masa [kg]</th>
          <th>Cena jednostkowa [PLN/kg]</th>
          <th>Kwota [PLN]</th>
          <th>Numer odstrzalu</th>
          <th>Data dodania</th>
          <th>Uwagi</th>
          <th>Zaakceptowane</th>
        </tr>
      </thead>
      <tbody>
    

<?php
    $id = $_SESSION['ID'];
    $rezultat = $polaczenie->query("
        SELECT Gatunek, Masa, Cena_jednostkowa, Numer_odstrzalu, Data_dodania, Uwagi, Zaakceptowane 
        FROM ZDOBYCZE LEFT OUTER JOIN ODSTRZALY ON ZDOBYCZE.ID_ODSTRZALY = ODSTRZALY.ID
        LEFT OUTER JOIN ZWIERZYNA ON ZDOBYCZE.ID_ZWIERZYNA = ZWIERZYNA.ID
        WHERE ZDOBYCZE.ID_MYSLIWI = '$id'
        ORDER BY Data_dodania DESC");
        while ($wiersz = $rezultat->fetch_assoc()) 
        {

            echo   '<tr>
                    <td>'.$wiersz['Gatunek'].'</td>
                    <td>'.$wiersz['Masa'].'</td>
                    <td>'.$wiersz['Cena_jednostkowa'].'</td>
                    <td>'.$wiersz['Cena_jednostkowa']*$wiersz['Masa'].'</td>
                    <td>'.$wiersz['Numer_odstrzalu'].'</td>
                    <td>'.$wiersz['Data_dodania'].'</td>
                    <td>'.$wiersz['Uwagi'].'</td>';
                    if($wiersz['Zaakceptowane'] == 0)
                    {
                        $wiersz['Zaakceptowane'] = "Oczekuje";
                        echo '<td style="background: rgba(255, 247, 135, .7);">'.$wiersz['Zaakceptowane'].'</td>';
                    
                    }
                    else
                    {
                        $wiersz['Zaakceptowane'] = "Tak";
                        echo '<td style="background: rgba(223, 255, 173, .7);">'.$wiersz['Zaakceptowane'].'</td>';
                    }

            echo    '</tr>';
       }
       $rezultat->free_result();
    $polaczenie->close();
?>

      </tbody>
    </table>

                </div>
			</fieldset>

          </div>

		</div>




</body>
</html>