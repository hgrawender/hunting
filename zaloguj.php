<?php

	session_start();
	
	if ((!isset($_POST['email'])) || (!isset($_POST['haslo'])))
	{
		header('Location: index.php');
		exit();
	}

	require_once "connect.php";

	$polaczenie = @new mysqli($host, $db_user, $db_password, $db_name);
	$polaczenie->set_charset("utf8");
	
	if ($polaczenie->connect_errno!=0)
	{
		echo "Error: ".$polaczenie->connect_errno;
	}
	else
	{
		$email = $_POST['email'];
		$haslo = $_POST['haslo'];
		
		$email = htmlentities($email, ENT_QUOTES, "UTF-8");
		$haslo = htmlentities($haslo, ENT_QUOTES, "UTF-8");
	
		if ($rezultat = @$polaczenie->query(
		sprintf("SELECT * FROM mysliwi WHERE email='%s' AND haslo='%s'",
		mysqli_real_escape_string($polaczenie,$email),
		mysqli_real_escape_string($polaczenie,$haslo))))
		{
			$ilu_userow = $rezultat->num_rows;
			if($ilu_userow>0)
			{
				$_SESSION['zalogowany'] = true;
				
				$wiersz = $rezultat->fetch_assoc();
				$_SESSION['ID'] = $wiersz['ID'];
				$_SESSION['Imie'] = $wiersz['Imie'];
				$_SESSION['Nazwisko'] = $wiersz['Nazwisko'];
				$_SESSION['Email'] = $wiersz['Email'];
				$_SESSION['Telefon'] = $wiersz['Telefon'];
				$_SESSION['Ulica'] = $wiersz['Ulica'];
				$_SESSION['Miejscowosc'] = $wiersz['Miejscowosc'];
				$_SESSION['Kod_pocztowy'] = $wiersz['Kod_pocztowy'];
				$_SESSION['Data_dolaczenia'] = $wiersz['Data_dolaczenia'];
				$_SESSION['Uprawnienia'] = $wiersz['Uprawnienia'];
				
				unset($_SESSION['blad']);
				$rezultat->free_result();
				header('Location: profil.php');
				
			} else {
				
				$_SESSION['blad'] = '<span style="color:red">Nieprawidłowy email lub hasło! &nbsp; Ciekawe które :D</span>';
				header('Location: index.php');
				
			}
			
		}
		
		$polaczenie->close();
	}
	
?>