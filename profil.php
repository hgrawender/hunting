<?php

	session_start();
	
	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
	
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Zalogowano</title>

    <script src="js/jquery-3.2.1.min.js"></script>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<script src="js/bootstrap.min.js"></script>
    <script src="bootstrap-notify-master/bootstrap-notify.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Exo" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="main.css"/>



</head>

<body>
<?php include 'header.php'; ?>


<?php
    if(isset($_GET['z']) && $_GET['z']==1)
    {
      echo  "<script>   
            $.notify({
                message: '<strong>Sukces!</strong> Dane zostały zmienione!',
            },{
                type: 'success',
                delay: 500,
                height: 50,
                align: 'center',
                animate: {
                enter: 'animated lightSpeedIn',
                exit: 'animated lightSpeedOut'},
                placement: { align: 'center' },
                offset: { x: 500 }
            });
            </script>";
    }
?>

	<div class="container">
		<h1>>Zalogowany</h1>
		<div class="well form-horizontal">
			<fieldset>
			<?php
				echo '<legend>Witaj '.$_SESSION['Imie'].', twoje dane to:</legend>';	
			?>
	
					<div class="col-md-10 col-md-offset-1" >
			  				<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
			  		</div>
				<div class="col-md-2 col-lg-2 " align="center">
                </div>



                <div class=" col-md-9 col-lg-9 "> 

				<table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>Imie:</td>
<?php
                       echo '<td>'.$_SESSION['Imie'].'</td>';
?>
                      </tr>
                      <tr>
                        <td>Nazwisko:</td>
<?php
                       echo '<td>'.$_SESSION['Nazwisko'].'</td>';
?>
                      </tr>
                      <tr>
                        <td>Email:</td>
<?php
                       echo '<td>'.$_SESSION['Email'].'</td>';
?>
                      </tr>
                        <tr>
                        <td>Telefon:</td>
<?php
                       echo '<td>'.$_SESSION['Telefon'].'</td>';
?>
                           
                      </tr>
                         <tr>
                             
                        <td>Ulica:</td>
<?php
                       echo '<td>'.$_SESSION['Ulica'].'</td>';
?>
                      </tr>
                        <tr>
                        <td>Miejscowość:</td>
<?php
                       echo '<td>'.$_SESSION['Miejscowosc'].'</td>';
?>
                      </tr>
                      <tr>
                        <td>Kod pocztowy:</td>
<?php
                       echo '<td>'.$_SESSION['Kod_pocztowy'].'</td>';
?>
                      </tr>
                        <td>Data dołączenia:</td>
<?php
                       echo '<td>'.$_SESSION['Data_dolaczenia'].'</td>';
?>
                           
                      </tr>
                      <tr>
                        <td>Uprawnienia:</td>
<?php
                       echo '<td>'.$_SESSION['Uprawnienia'].'</td>';
?>
                      </tr>
                    </tbody>
                  </table>

                </div>
                <span class="pull-right">
                	<a href="edytuj_profil.php" class="btn btn-warning">Edytuj dane</a>
                	<a href="logout.php" class="btn btn-danger">Wyloguj się!</a>
                </span>
			</fieldset>

          </div>

		</div>




</body>
</html>