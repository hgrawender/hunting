<?php

	session_start();
	
	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}

    require_once "connect.php";
    $polaczenie = new mysqli($host, $db_user, $db_password, $db_name);
    $polaczenie->set_charset("utf8");

?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Zalogowano</title>

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<script src="js/bootstrap.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Exo" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="main.css"/>
</head>

<body>
<?php include 'header.php'; ?>


	<div class="container">
		<h1>>Zalogowany</h1>
        <form class="well form-horizontal" action="edytuj_profil_skrypt.php" method="post">
			<fieldset>
			<?php
				echo '<legend>Witaj '.$_SESSION['Imie'].', twoje dane to:</legend>';	
			?>
	
					<div class="col-md-10 col-md-offset-1" >
			  				<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
			  		</div>
				<div class="col-md-2 col-lg-2 " align="center">
                </div>



                <div class=" col-md-9 col-lg-9 "> 

				<table class="table table-user-information table_edit" >
                    <tbody>
                      <tr>
                        <td class="col-xs-5">Imie:</td>
                        <td>
                                    <div class="col-xs-10">
                                        <?php echo '<input type="text" id="Imie" name="Imie" class="form-control input-sm" value="'.$_SESSION['Imie'].'">'; ?>
                                    </div>
                        </td>
                      </tr>
                      <tr>
                        <td class="col-xs-5">Nazwisko:</td>
                        <td>
                                    <div class="col-xs-10">
                                        <?php echo '<input type="text" id="Nazwisko" name="Nazwisko" class="form-control input-sm" value="'.$_SESSION['Nazwisko'].'">'; ?>
                                    </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-xs-5">Email:</td>
                        <td>
                                    <div class="col-xs-10">
                                        <?php echo '<input type="text" id="Email" name="Email" class="form-control input-sm" value="'.$_SESSION['Email'].'">'; ?>
                                    </div>
                        </td>
                      </tr>
                   
                    <tr>
                        <td class="col-xs-5">Telefon:</td>
                        <td>
                                    <div class="col-xs-10">
                                        <?php echo '<input type="text" id="Telefon" name="Telefon" class="form-control input-sm" value="'.$_SESSION['Telefon'].'">'; ?>
                                    </div>
                        </td>
                    </tr>
                
                             <tr>
                        <td class="col-xs-5">Ulica:</td>
                        <td>
                                    <div class="col-xs-10">
                                        <?php echo '<input type="text" id="Ulica" name="Ulica" class="form-control input-sm" value="'.$_SESSION['Ulica'].'">'; ?>
                                    </div>
                        </td>
                      </tr>
                        <tr>
                        <td class="col-xs-5">Miejscowość:</td>
                        <td>
                                    <div class="col-xs-10">
                                        <?php echo '<input type="text" id="Miejscowosc" name="Miejscowosc" class="form-control input-sm" value="'.$_SESSION['Miejscowosc'].'">'; ?>
                                    </div>
                        </td>
                      </tr>
                      <tr>
                        <td class="col-xs-5">Kod pocztowy:</td>
                        <td>
                                    <div class="col-xs-10">
                                        <?php echo '<input type="text" id="Kod_pocztowy" name="Kod_pocztowy" class="form-control input-sm" value="'.$_SESSION['Kod_pocztowy'].'">'; ?>
                                    </div>
                        </td>
                      </tr>
                        <td class="col-xs-5">Data dołączenia:</td>
                        <td>
                                    <div class="col-xs-10">
                                        <?php echo '<input type="text" id="Data_dolaczenia" name="Data_dolaczenia" class="form-control input-sm" value="'.$_SESSION['Data_dolaczenia'].'">'; ?>
                                    </div>
                           </td>
                      </tr>
                      <tr>
                        <td class="col-xs-5">Uprawnienia:</td>
<?php
                       echo '<td>'.$_SESSION['Uprawnienia'].'</td>';
?>
                      </tr>
                    </tbody>
                  </table>

                </div>
                <span class="pull-right">
                    <button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok"></span> &nbsp;Zatwierdź zmiany </button>&nbsp;
                	<a href="profil.php" class="btn btn-warning">Anuluj</a>
                </span>
			</fieldset>
        </form>
          </div>

		</div>


<?php
// echo "<br/><h3> Sekcja do testowania formularza - przekazane dane:</h3>";



//         if (isset($_POST['Imie']))
//         {
//             echo "<br/>Nazwa skupu: ";
//             echo $_POST['Imie'];
//             $Imie=$_POST['Imie'];

//             if (isset($_POST['Nazwisko']))
//             {
//                 echo "<br/>Nazwisko: ";
//                 echo $_POST['Nazwisko'];
//                 $Nazwisko=$_POST['Nazwisko'];

//                 if (isset($_POST['Email']))
//                 {
//                     echo "<br/>Email: ";
//                     echo $_POST['Email'];
//                     $Email=$_POST['Email'];

//                     if (isset($_POST['Kod_pocztowy']))
//                     {
//                         echo "<br/>kod pocztowy: ";
//                         echo $_POST['Kod_pocztowy'];
//                         $Kod_pocztowy=$_POST['Kod_pocztowy'];

//                         if (isset($_POST['Ulica']))
//                         {
//                             echo "<br/>Ulica: ";
//                             echo $_POST['Ulica'];
//                             $Ulica=$_POST['Ulica'];

//                             if (isset($_POST['Telefon']))
//                             {
//                             echo "<br/>Telefon: ";
//                             echo $_POST['Telefon'];
//                             $Telefon=$_POST['Telefon'];

//                                 if (isset($_POST['Miejscowosc']))
//                                 {
//                                 echo "<br/>Miejscowosc: ";
//                                 echo $_POST['Miejscowosc'];
//                                 $Miejscowosc=$_POST['Miejscowosc'];

//                                     if (isset($_POST['Data_dolaczenia']))
//                                     {
//                                     echo "<br/>Data_dolaczenia: ";
//                                     echo $_POST['Data_dolaczenia'];
//                                     $Data_dolaczenia=$_POST['Data_dolaczenia'];

//                                     $id = $_SESSION['ID'];
//                                     $polaczenie->query("UPDATE MYSLIWI SET Imie = '$Imie', Nazwisko='$Nazwisko', Ulica='$Ulica', Miejscowosc='$Miejscowosc', Kod_pocztowy='$Kod_pocztowy', Telefon= '$Telefon', Email='$Email', Data_dolaczenia='$Data_dolaczenia' WHERE ID = $id;");


//                                     $_SESSION['Imie'] = $_POST['Imie'];
//                                     $_SESSION['Nazwisko'] = $_POST['Nazwisko'];
//                                     $_SESSION['Ulica'] = $_POST['Ulica'];
//                                     $_SESSION['Kod_pocztowy'] = $_POST['Kod_pocztowy'];
//                                     $_SESSION['Telefon'] = $_POST['Telefon'];
//                                     $_SESSION['Email'] = $_POST['Email'];
//                                     $_SESSION['Data_dolaczenia'] = $_POST['Data_dolaczenia'];


//                                     echo "UPDATE MYSLIWI SET Imie = '$Imie', Nazwisko='$Nazwisko', Ulica='$Ulica', Miejscowosc='$Miejscowosc', Kod_pocztowy='$Kod_pocztowy', Telefon= '$Telefon', Email='$Email', Data_dolaczenia='$Data_dolaczenia' WHERE ID = $id;";


//                                     }else echo "Nazwa skupu nie została ustawiona";
//                                 }else echo "Email nie został ustawiony";
//                             }else echo "Telefon nie został ustawiony";
//                         }else echo "Nip nie został ustawiony";
//                     }else echo "Kod pocztowy nie został ustawiony";
//                 }else echo "Email nie zostało ustawione";
//             }else echo "Adres nie został ustawiony";
//         }else echo "Nazwa skupu nie została ustawiona";




// $polaczenie->close();

?>



</body>
</html>