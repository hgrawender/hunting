<?php

	session_start();
	
	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
	
?>

<?php
    require_once "connect.php";
    $polaczenie = new mysqli($host, $db_user, $db_password, $db_name);
    $polaczenie->set_charset("utf8");
?>

<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Zalogowano</title>

	<link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>


	<link href="https://fonts.googleapis.com/css?family=Exo" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="main.css"/>

   <script> function usun_skup(id)
    { 
        if(confirm("Jesteś pewny że chcesz usunąć ten skup?")==true)
        window.location="usun_skup.php?id="+id;
        return false;
    }</script> 

</head>

<body>
<?php include 'header.php'; ?>


	<div class="container">
		<h1>>Skupy</h1>
		<div class="well form-horizontal">
			<fieldset>

			<legend>Skupy znajdujące się w bazie:</legend>	


    <div class=" col-md-12 col-lg-12 "> 
  
<table class="table table-condensed" style="border-collapse:collapse;">
    <thead>
        <tr>
            <th>#</th>
            <th>Nazwa</th>
            <th>Ulica</th>
        </tr>
    </thead>
    <tbody>



<?php
   

       // $polaczenie->query("DELETE FROM SKUPY WHERE ID='".$ID."'");
    // $sql="DELETE FROM bs_reservations WHERE id='".$orderID."'";
    // $result=mysql_query($sql) or die("oopsy, error when tryin to delete events 2");


    $rezultat = $polaczenie->query("SELECT ID, Nazwa, Ulica, Miejscowosc, Kod_pocztowy, NIP, Telefon , Email FROM SKUPY");
    while ($wiersz = $rezultat->fetch_assoc()) 
    {


        echo '<tr data-toggle="collapse" data-target="#'.$wiersz['ID'].'" class="accordion-toggle">
            <td>'.$wiersz['ID'].'</td>
            <td>'.$wiersz['Nazwa'].'</td>
            <td>'.$wiersz['Ulica'].'</td>
            <td><span><i class="glyphicon glyphicon-chevron-down"></i></span></td>
        </tr>
        <tr >
            <td></td>
                <td colspan="3">
                    <div id="'.$wiersz['ID'].'" class="collapse">
                            - Miejscowosc: '.$wiersz['Miejscowosc'].'<br /> 
                            - Kod pocztowy: '.$wiersz['Kod_pocztowy'].'<br /> 
                            - NIP: '.$wiersz['NIP'].'<br /> 
                            - Telefon: '.$wiersz['Telefon'].'<br /> 
                            - E-mail: '.$wiersz['Email'].'<br /> 

                            <span class="pull-right" style="margin-right: 5%;">
                                <!--<a href="#" class="btn btn-warning">Edytuj dane (X)</a>-->
                                <a class="btn btn-danger" onclick="usun_skup('.$wiersz['ID'].')">Usuń skup!</a>
                            </span>
                    </div>
                    </td>
           
        </tr>';
    }
    $rezultat->free_result();


?>
    </tbody>
</table>


                </div>
                <div  style= "width: 100%;"><div style="text-align: center;  margin-right: auto; margin-left: auto;">
                    <a href="dodaj_skup.php" class="btn btn-success" style="text-align: center;  margin-right: auto; margin-left: auto;"><span class="glyphicon glyphicon-arrow-right"></span> &nbsp;Dodaj skup</a>
                </div></div>
			</fieldset>

          </div>

		</div>


    <script type="text/javascript">
         $('.collapse').on('show.bs.collapse', function () {
             $('.collapse.in').each(function(){
                $(this).collapse('hide');
            });
        });
    </script>




<?php
    $polaczenie->close();
?>
</body>
</html>