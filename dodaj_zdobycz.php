<?php
	session_start();
	require_once "connect.php";
	$polaczenie = new mysqli($host, $db_user, $db_password, $db_name);
	$polaczenie->set_charset("utf8");
?>


<!DOCTYPE html>
<html lang="pl">
<head>
	<meta charset="UTF-8">
	<title>Dodaj zdobycz! - PHP</title>
	

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<script src="js/bootstrap.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Exo" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="main.css"/>


<script type="text/javascript">
	function pokaz_cene()
	{
		var wybrana_klasa = document.getElementById('klasa').value;
		var wybrana_zwierzyna = document.getElementById('zwierzyna').value;
		var wybrana_cena = document.getElementById(wybrana_klasa+wybrana_zwierzyna).textContent;
		document.getElementById('cena_div').innerHTML = '<b>'+wybrana_cena+' PLN/kg</b> - sugerowana cena koła łowieckiego przy wybranej klasie i gatunku';
	}

	function pokaz_kwote()
	{
		var masa = document.getElementById('masa').value;
		var cena= document.getElementById('cena').value;
		var kwota = masa * cena;
		kwota = kwota.toFixed(2);
		document.getElementById('kwota').value = kwota;
	}

	function disable_skupy()
	{
		document.getElementById("skupy").disabled = uzytek_wlasny.checked;
	};
</script>
</head>
<body>

<?php include 'header.php'; ?>




	
	<div class="container">
	<h1>>Dodaj zdobycz</h1>
    <form class="well form-horizontal" action=" " method="post" >
	<fieldset>
		<legend>Proszę wypełnij poniższy formularz:</legend>	

		<div class="form-group"> 
  			<label class="col-md-4 control-label">Myśliwy:</label>
    		<div class="col-md-4 selectContainer">
    			<div class="input-group">
        			<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
        			<select name="mysliwi" class="form-control selectpicker">

<?php

	$rezultat = $polaczenie->query("SELECT id, Imie, Nazwisko FROM MYSLIWI");

       while ($wiersz = $rezultat->fetch_assoc()) 
       {
         echo "\n<option value=\"".$wiersz['id']."\"> ".$wiersz['Imie']." ".$wiersz['Nazwisko']."</option>";
       }
    $rezultat->free_result();

?>
					</select> 
				</div>
			</div>
		</div>

	
	<div class="form-group">
 		 <label class="col-md-4 control-label" >Numer odstrzału:</label> 
    	<div class="col-md-4 inputGroupContainer">
    		<div class="input-group">
  				<span class="input-group-addon"><i class="glyphicon glyphicon-tags"></i></span>
  					<input type="number" id="odstrzal_nr" name="odstrzal_nr" class="form-control" placeholder="...">
    		</div>
  		</div>
	</div>

<hr>	
	<div class="form-group"> 
  		<label class="col-md-4 control-label">Podaj rodzaj zwierzyny:</label>
    		<div class="col-md-4 selectContainer">
    			<div class="input-group">
        			<span class="input-group-addon"><i class="glyphicon glyphicon-piggy-bank"></i></span>
						<select name="zwierzyna" id="zwierzyna" onclick="pokaz_cene();" class="form-control selectpicker">
							<option value="1">Dzik</option>
							<option value="2">Jeleń</option>
							<option value="3">Sarenka</option>
							<option value="4">Daniel</option>
							<option value="5">Muflon</option>
						</select>
				</div>
			</div>
	</div>
	

	<div class="form-group">
 		 <label class="col-md-4 control-label" >Masa zdobyczy:</label> 
    		<div class="col-md-4 inputGroupContainer">
    			<div class="input-group">
  					<span class="input-group-addon"><i class="glyphicon glyphicon-scale"></i></span>
  					<input type="number" id="masa" name="masa" min="0" max="300" step="0.01" onchange="pokaz_kwote();" class="form-control">
  					<span class="input-group-addon" id="basic-addon2">kg</span>
    			</div>
  			</div>
	</div>


	<div class="form-group"> 
  		<label class="col-md-4 control-label">Klasa uszkodzenia tuszy:</label>
    		<div class="col-md-4 selectContainer">
    			<div class="input-group">
        			<span class="input-group-addon"><i class="glyphicon glyphicon-star"></i></span>
						<select id="klasa" name="klasa" onclick="pokaz_cene();" class="form-control selectpicker">
							<option value="1">1 klasa</option>
							<option value="2">2 klasa</option>
							<option value="3">3 klasa</option>
							<option value="4">Poza wyborem</option>
						</select>
				</div>
			</div>
	</div>


		<div class="form-group"> 
  			<label class="col-md-4 control-label">Obwód pozyskania:</label>
    		<div class="col-md-4 selectContainer">
    			<div class="input-group">
        			<span class="input-group-addon"><i class="glyphicon glyphicon-tree-conifer"></i></span>
        			<select name="obwod" class="form-control selectpicker">

<?php
	$rezultat = $polaczenie->query("SELECT id, obwod FROM miejsca_pozyskania");
    while ($wiersz = $rezultat->fetch_assoc()) 
    {
        echo "\n<option value=\"".$wiersz['id']."\"> ".$wiersz['obwod'].' </option>';
    }
    $rezultat->free_result();
?>
					</select> 
				</div>
			</div>
		</div>

	<div class="form-group"> 
  		<label class="col-md-4 control-label">Data odstrzału: </label>
    		<div class="col-md-4 selectContainer">
    			<div class="input-group">
        			<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
<?php
					$today = date("Y-m-j");
        			echo '<input type="date" name="data_odstrz" class="form-control" value="'.$today.'">'
 ?>
	    			</div>
  			</div>
	</div>

	<div class="form-group"> 
  		<label class="col-md-4 control-label">Użytek własny?</label>
    		<div class="col-md-1">
    			<div class="radio">
    			<input type="checkbox" id="uzytek_wlasny" name="uzytek_wlasny" onchange="disable_skupy();" unchecked>	
				</div>  
			</div>
	</div>
	
	<div class="form-group"> 
  		<label class="col-md-4 control-label">Wybrany skup:</label>
    		<div class="col-md-4 selectContainer">
    			<div class="input-group">
        			<span class="input-group-addon"><i class="glyphicon glyphicon-list-alt"></i></span>
        			<select id="skupy" name="skupy" class="form-control selectpicker">

<?php 
	$rezultat = $polaczenie->query("SELECT id, nazwa FROM skupy");

       while ($wiersz = $rezultat->fetch_assoc()) 
       {
         echo '<option value=" '.$wiersz['id'].' "> '.$wiersz['nazwa'].' </option>';
       }
	$rezultat->free_result();
?>
						</select> 
				</div>
			</div>
		</div>


	<div class="form-group">
 		 <label class="col-md-4 control-label" >Cena za kg:</label> 
    		<div class="col-md-4 inputGroupContainer">
    			<div class="input-group">
  					<span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span>
  					<input type="number" id="cena" name="cena" min="0" max="300" step="0.01" value="0.00" onchange="pokaz_kwote();" class="form-control">
  					<span class="input-group-addon" id="basic-addon2">PLN/kg</span>
    			</div>
  			</div>
  			<div id="cena_div"></div>
	</div>

	
<?php
	$rezultat = $polaczenie->query("SELECT cena_zwierzyna_klasa.id_klasy, cena_zwierzyna_klasa.id_zwierzyna, zwierzyna.id, cena_zwierzyna_klasa.cena FROM cena_zwierzyna_klasa INNER JOIN zwierzyna ON cena_zwierzyna_klasa.id_zwierzyna = zwierzyna.id");

        while ($wiersz = $rezultat->fetch_assoc()) 
       {
         	echo "<div id=\"".$wiersz['id_klasy'].$wiersz['id_zwierzyna']."\" style=\"display: none\">".$wiersz['cena']."</div>\n\n";
       }
    $rezultat->free_result();
?>
	
	<div class="form-group">
 		 <label class="col-md-4 control-label" >Łączna kwota: </label> 
    		<div class="col-md-4 inputGroupContainer">
    			<div class="input-group">
  					<span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span>
  					<input type="number" id="kwota" name="kwota" min="0" class="form-control" step="0.01" placeholder="Liczenie automatyczne">
    				<span class="input-group-addon" id="basic-addon2">PLN</span>
    			</div><span>(masa x cena za kg)</span>
  			</div>
	</div>

	<div class="form-group">
	  <label class="col-md-4 control-label">Uwagi:</label>  
	    <div class="col-md-4 inputGroupContainer">
	    	<div class="input-group">
		        <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
		  		<textarea rows="4" type="text" id="uwagi" name="uwagi" maxlength="200" size="80" class="form-control" placeholder="Max. 200 znaków"></textarea>
	    	</div>
	  	</div>
	</div>

	<div class="form-group">
	  <label class="col-md-5 control-label "></label>
	  	<div class="col-md-4" >
	    	<button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok"></span> &nbsp;Dodaj zdobycz </button>
	  	</div>
	</div>

</fieldset>
</form>
</div>


<?php
echo "<br/><h3> Sekcja do testowania formularza - przekazane dane:</h3><h4>zapytaj o problem z enterem</h4>";
if (isset($_POST['mysliwi']))
	{
		echo "<br/>Myśliwy: ";
		echo $_POST['mysliwi'];
		$mysliwi=$_POST['mysliwi'];
	}
	if (isset($_POST['odstrzal_nr']))
	{
		echo "<br/>Numer odstrzalu: ";
		echo $_POST['odstrzal_nr'];
		$odstrzal_nr=$_POST['odstrzal_nr'];
	}

	if (isset($_POST['mysliwi']) && isset($_POST['odstrzal_nr']))
	{

		$rezultat = $polaczenie->query("SELECT ID FROM ODSTRZALY WHERE ID_MYSLIWI='$mysliwi' AND Numer_odstrzalu='$odstrzal_nr'");
		echo "SELECT ID FROM ODSTRZALY WHERE ID_MYSLIWI='$mysliwi' AND Numer_odstrzalu='$odstrzal_nr'";
		if($rezultat){
		
		$ile_odstrzalow = mysqli_num_rows($rezultat);
		echo "<br/>ilość: ".$ile_odstrzalow;
		
		if($ile_odstrzalow>0)
		{

			$wiersz = $rezultat->fetch_assoc();
			$rezultat->free_result();
			$odstrzal = $wiersz['ID'];
			echo "<br/> Odstrzał: ".$odstrzal;

		if (isset($_POST['zwierzyna']))
		{
			echo "<br/>Rodzaj zwierzyny: ";
			echo $_POST['zwierzyna'];
			$zwierzyna=$_POST['zwierzyna'];
		}
		if (isset($_POST['masa']))
		{
			echo "<br/>Masa: ";
			echo $_POST['masa'];
			$masa=$_POST['masa'];
		}
		if (isset($_POST['klasa']))
		{
			echo "<br/>Klasa: ";
			echo $_POST['klasa'];
			$klasa=$_POST['klasa'];
		}
		if (isset($_POST['obwod']))
		{
			echo "<br/>Obwód: ";
			echo $_POST['obwod'];
			$obwod=$_POST['obwod'];
		}
		if (isset($_POST['data_odstrz']))
		{
			echo "<br/>Data odstrzału: ";
			echo $_POST['data_odstrz'];
			$data_odstrz=$_POST['data_odstrz'];
		}
		if (isset($_POST['uzytek_wlasny']))
		{
			echo "<br/>uzytek_wlasny: ";
			$_POST['uzytek_wlasny'] = 1;
			echo $_POST['uzytek_wlasny'];
			$uzytek_wlasny=$_POST['uzytek_wlasny'];
		}
		if (isset($_POST['skupy']))
		{

			echo "<br/>skupy: ";
			echo $_POST['skupy'];
			$_POST['uzytek_wlasny'] = 0;
			echo "<br/>uzytek_wlasny: ".$_POST['uzytek_wlasny'];
			$uzytek_wlasny=$_POST['uzytek_wlasny'];
			$skupy=$_POST['skupy'];
		}

		if (isset($_POST['cena']))
		{
			echo "<br/>cena: ";
			echo $_POST['cena'];
			$cena=$_POST['cena'];
		}
		if (isset($_POST['kwota']))
		{
			echo "<br/>kwota: ";
			echo $_POST['kwota'];
			$cena=$_POST['cena'];
		}
		if (isset($_POST['kwota']))
		{
			echo "<br/>Uwagi: ";
			echo $_POST['uwagi'];
			$uwagi=$_POST['uwagi'];
		}

		$date = date('Y-m-d H:i:s');			
		echo "<br/>Data dodania: ".$date."<br/><br/>";

		if (isset($_POST['kwota'])){
			$polaczenie->query("INSERT INTO ZDOBYCZE (ID, ID_ZWIERZYNA, ID_MYSLIWI, ID_ODSTRZALY, ID_MIEJSCA_POZYSKANIA, ID_SKUPY, ID_KLASY, Masa, Uzytek_wlasny, Cena_jednostkowa, Data_odstrzalu, Data_dodania, Uwagi) 
			VALUES (DEFAULT, $zwierzyna, $mysliwi, $odstrzal, $obwod, $skupy, $klasa, $masa, $uzytek_wlasny, $cena, '$data_odstrz', NOW(), '$uwagi')");

				echo "INSERT INTO ZDOBYCZE (ID, ID_ZWIERZYNA, ID_MYSLIWI, ID_ODSTRZALY, ID_MIEJSCA_POZYSKANIA, ID_SKUPY, ID_KLASY, Masa, Uzytek_wlasny, Cena_jednostkowa, Data_odstrzalu, Data_dodania, Uwagi) 
			VALUES (DEFAULT, $zwierzyna, $mysliwi, $odstrzal, $obwod, $skupy, $klasa, $masa, $uzytek_wlasny, $cena, '$data_odstrz', NOW(), '$uwagi')";
		}
	}
	else
		{
			echo "<br/>NUMER ODSTRZALU nie zgadza się!";
		}

	}else echo "zły rezultat";
}else echo "zmnienne nie zostały ustawione";
$polaczenie->close();
?>

</body>
</html>