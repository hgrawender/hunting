INSERT INTO ZWIERZYNA (ID, Gatunek) VALUES (1, 'Dzik');
INSERT INTO ZWIERZYNA (ID, Gatunek) VALUES (2, 'Jelen');
INSERT INTO ZWIERZYNA (ID, Gatunek) VALUES (3, 'Sarenka');
INSERT INTO ZWIERZYNA (ID, Gatunek) VALUES (4, 'Daniel');
INSERT INTO ZWIERZYNA (ID, Gatunek) VALUES (5, 'Muflon');


INSERT INTO KLASY (ID, Nazwa) VALUES (1, '1');
INSERT INTO KLASY (ID, Nazwa) VALUES (2, '2');
INSERT INTO KLASY (ID, Nazwa) VALUES (3, '3');
INSERT INTO KLASY (ID, Nazwa) VALUES (4, 'PW');


INSERT INTO CENA_ZWIERZYNA_KLASA (ID, ID_ZWIERZYNA, ID_KLASY, Cena) VALUES (1, 1, 1, 11);
INSERT INTO CENA_ZWIERZYNA_KLASA (ID, ID_ZWIERZYNA, ID_KLASY, Cena) VALUES (2, 1, 2, 12);
INSERT INTO CENA_ZWIERZYNA_KLASA (ID, ID_ZWIERZYNA, ID_KLASY, Cena) VALUES (3, 1, 3, 13);
INSERT INTO CENA_ZWIERZYNA_KLASA (ID, ID_ZWIERZYNA, ID_KLASY, Cena) VALUES (4, 1, 4, 14);

INSERT INTO CENA_ZWIERZYNA_KLASA (ID, ID_ZWIERZYNA, ID_KLASY, Cena) VALUES (5, 2, 1, 21);
INSERT INTO CENA_ZWIERZYNA_KLASA (ID, ID_ZWIERZYNA, ID_KLASY, Cena) VALUES (6, 2, 2, 22);
INSERT INTO CENA_ZWIERZYNA_KLASA (ID, ID_ZWIERZYNA, ID_KLASY, Cena) VALUES (7, 2, 3, 23);
INSERT INTO CENA_ZWIERZYNA_KLASA (ID, ID_ZWIERZYNA, ID_KLASY, Cena) VALUES (8, 2, 4, 24);

INSERT INTO CENA_ZWIERZYNA_KLASA (ID, ID_ZWIERZYNA, ID_KLASY, Cena) VALUES (9, 3, 1, 31);
INSERT INTO CENA_ZWIERZYNA_KLASA (ID, ID_ZWIERZYNA, ID_KLASY, Cena) VALUES (10, 3, 2, 32);
INSERT INTO CENA_ZWIERZYNA_KLASA (ID, ID_ZWIERZYNA, ID_KLASY, Cena) VALUES (11, 3, 3, 33);
INSERT INTO CENA_ZWIERZYNA_KLASA (ID, ID_ZWIERZYNA, ID_KLASY, Cena) VALUES (12, 3, 4, 34);

INSERT INTO CENA_ZWIERZYNA_KLASA (ID, ID_ZWIERZYNA, ID_KLASY, Cena) VALUES (13, 4, 1, 41);
INSERT INTO CENA_ZWIERZYNA_KLASA (ID, ID_ZWIERZYNA, ID_KLASY, Cena) VALUES (14, 4, 2, 42);
INSERT INTO CENA_ZWIERZYNA_KLASA (ID, ID_ZWIERZYNA, ID_KLASY, Cena) VALUES (15, 4, 3, 43);
INSERT INTO CENA_ZWIERZYNA_KLASA (ID, ID_ZWIERZYNA, ID_KLASY, Cena) VALUES (16, 4, 4, 44);

INSERT INTO CENA_ZWIERZYNA_KLASA (ID, ID_ZWIERZYNA, ID_KLASY, Cena) VALUES (17, 5, 1, 51);
INSERT INTO CENA_ZWIERZYNA_KLASA (ID, ID_ZWIERZYNA, ID_KLASY, Cena) VALUES (18, 5, 2, 52);
INSERT INTO CENA_ZWIERZYNA_KLASA (ID, ID_ZWIERZYNA, ID_KLASY, Cena) VALUES (19, 5, 3, 53);
INSERT INTO CENA_ZWIERZYNA_KLASA (ID, ID_ZWIERZYNA, ID_KLASY, Cena) VALUES (20, 5, 4, 54);

INSERT INTO SKUPY (ID, Nazwa, Ulica, Miejscowosc, Kod_pocztowy, NIP, Telefon, Email) 
VALUES (DEFAULT, 'Darz Bór s.c. Handel dziczyzną', 'Żeromskiego 54', 'Łódź', '90-626', '7270128392', '426373352', 'darzborXXX@gmail.com');
INSERT INTO SKUPY (ID, Nazwa, Ulica, Miejscowosc, Kod_pocztowy, NIP, Telefon, Email) 
VALUES (DEFAULT, 'Weles Sp. z o.o.', 'Bartoszycka 12', 'Poznań', '60-434', '7811107468', '48618488885', 'weles@weles.pl');
INSERT INTO SKUPY (ID, Nazwa, Ulica, Miejscowosc, Kod_pocztowy, NIP, Telefon, Email) 
VALUES (DEFAULT, 'Wiliński K. Skup dziczyzny i runa leśnego', 'Strzelnicza 10', 'Piotrków Trybunalski', '97-300', ' 5971194506', '447322064', 'wilinskiXXX@gmail.com');

INSERT INTO MYSLIWI (ID, Imie, Nazwisko, Ulica, Miejscowosc, Kod_pocztowy, Telefon, Email, Data_dolaczenia, Haslo, Zdjecie, Uprawnienia) 
VALUES (1, 'S', 'Gr', 'Sp', 'Łódź', '90-156', '667893423', 's.graw@gmail.com', '','slaw1234','img1.jpg', 1);
INSERT INTO MYSLIWI (ID, Imie, Nazwisko, Ulica, Miejscowosc, Kod_pocztowy, Telefon, Email, Data_dolaczenia, Haslo, Zdjecie, Uprawnienia) 
VALUES (2, 'Hubert', 'Gr', 'Sp', 'Łódź', '90-156', '773456385', 'h.graw@gmail.com', '','hub1234','img2.jpg', 0);
INSERT INTO MYSLIWI (ID, Imie, Nazwisko, Ulica, Miejscowosc, Kod_pocztowy, Telefon, Email, Data_dolaczenia, Haslo, Zdjecie, Uprawnienia) 
VALUES (3, 'Jan', 'Kowalski', 'Piotrkowska 107', 'Łódź', '93-212', '773456385', 'kowalski@wp.pl', '','haslo123','img3.jpg', 0);
INSERT INTO MYSLIWI (ID, Imie, Nazwisko, Ulica, Miejscowosc, Kod_pocztowy, Telefon, Email, Data_dolaczenia, Haslo, Zdjecie, Uprawnienia) 
VALUES (4, 'Michał', 'Nowak', 'Kwiatowa 13a', 'Łódź', '90-156', '554090352', 'nowak88@op.pl', '','dzikidzik45','img4.jpg', 0);
INSERT INTO MYSLIWI (ID, Imie, Nazwisko, Ulica, Miejscowosc, Kod_pocztowy, Telefon, Email, Data_dolaczenia, Haslo, Zdjecie, Uprawnienia) 
VALUES (5, 'Stefan', 'Ważniak', 'Zabłockiego 8', 'Krośniewice', '43-126', '534029452', 'wazniacki3@interia.pl', '','lowczy84','img5.jpg', 2);

INSERT INTO MIEJSCA_POZYSKANIA (ID, Obwod) 
VALUES (1, '13 Nadleśnictwo Kutno');
INSERT INTO MIEJSCA_POZYSKANIA (ID, Obwod) 
VALUES (2, '115 Nadleśnictwo Poddębice');

INSERT INTO ODSTRZALY (ID, ID_MYSLIWI, Numer_odstrzalu, Data_wydania, Data_waznosci, Wykorzystany) 
VALUES (DEFAULT, 1, 224, '2017-04-02', '2017-09-02', DEFAULT);

INSERT INTO ODSTRZALY (ID, ID_MYSLIWI, Numer_odstrzalu, Data_wydania, Data_waznosci, Wykorzystany) 
VALUES (DEFAULT, 2, 124, '2017-04-02', '2017-09-02', DEFAULT);
